#include <string>
#include <vector>
#include <iostream>
using namespace std;

class GF_2{
	protected:
		string n;

	public:
		vector<int> digits;
		GF_2();
		GF_2(string s);
		GF_2 gen_set();
		GF_2 remove_zeros();
		~GF_2();
		GF_2& constZero();
		GF_2& constOne();
};

GF_2::GF_2(){

}

GF_2::GF_2 (string n){
	for(unsigned int i = 0; i <= n.length(); i++){
			if(n[i] == '0'){ digits.push_back(0); digits.push_back(0); digits.push_back(0); digits.push_back(0); }
			if(n[i] == '1'){ digits.push_back(0); digits.push_back(0); digits.push_back(0); digits.push_back(1); }
			if(n[i] == '2'){ digits.push_back(0); digits.push_back(0); digits.push_back(1); digits.push_back(0); }
			if(n[i] == '3'){ digits.push_back(0); digits.push_back(0); digits.push_back(1); digits.push_back(1); }
			if(n[i] == '4'){ digits.push_back(0); digits.push_back(1); digits.push_back(0); digits.push_back(0); }
			if(n[i] == '5'){ digits.push_back(0); digits.push_back(1); digits.push_back(0); digits.push_back(1); }
			if(n[i] == '6'){ digits.push_back(0); digits.push_back(1); digits.push_back(1); digits.push_back(0); }
			if(n[i] == '7'){ digits.push_back(0); digits.push_back(1); digits.push_back(1); digits.push_back(1); }
			if(n[i] == '8'){ digits.push_back(1); digits.push_back(0); digits.push_back(0); digits.push_back(0); }
			if(n[i] == '9'){ digits.push_back(1); digits.push_back(0); digits.push_back(0); digits.push_back(1); }
			if((n[i] == 'A') || (n[i] == 'a')){ digits.push_back(1); digits.push_back(0); digits.push_back(1); digits.push_back(0); }
			if((n[i] == 'B') || (n[i] == 'b')){ digits.push_back(1); digits.push_back(0); digits.push_back(1); digits.push_back(1); }
			if((n[i] == 'C') || (n[i] == 'c')){ digits.push_back(1); digits.push_back(1); digits.push_back(0); digits.push_back(0); }
			if((n[i] == 'D') || (n[i] == 'd')){ digits.push_back(1); digits.push_back(1); digits.push_back(0); digits.push_back(1); }
			if((n[i] == 'E') || (n[i] == 'e')){ digits.push_back(1); digits.push_back(1); digits.push_back(1); digits.push_back(0); }
			if((n[i] == 'F') || (n[i] == 'f')){ digits.push_back(1); digits.push_back(1); digits.push_back(1); digits.push_back(1); }
	}
}

GF_2 GF_2::gen_set(){
	for(int i = 0; i <= 233; i++){
		this->digits.push_back(0);
	}
	this->digits[0] = 1;
	this->digits[224] = 1;
	this->digits[229] = 1;
	this->digits[232] = 1;
	this->digits[233] = 1;
	return *this;
}

GF_2 GF_2::remove_zeros(){
	if(this->digits.size() == 1)return *this;
	else{
		while(this->digits[0] == 0 && this->digits.size() != 1){
			this->digits.erase(this->digits.begin());
		}
	return *this;
	}
}
GF_2::~GF_2(){

}

GF_2& GF_2::constZero(){
	GF_2* first_f = new GF_2("0");
	return *first_f;
}

GF_2& GF_2::constOne(){
	GF_2* first_f = new GF_2("1");
	return *first_f;
}

GF_2 add(GF_2 first_f, GF_2 second_f){
	GF_2 result;
	int d;
	if(first_f.digits.size() > second_f.digits.size()){
		d = first_f.digits.size() - second_f.digits.size();
		for(int i = 0; i < d; i++)
		{
			second_f.digits.emplace(second_f.digits.begin(), 0);
		}
	}
	if(first_f.digits.size() < second_f.digits.size()){
		d = second_f.digits.size() - first_f.digits.size();
		for(int i = 0; i < d; i++)
		{
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	}
	for(unsigned int i = 0; i < first_f.digits.size(); i++){
		result.digits.push_back((first_f.digits[i] ^ second_f.digits[i]));
	}
	return result;
}

string convert_to_str(GF_2 first_f){
	string result;
	if(first_f.digits.size() % 4 == 1)
		for(unsigned int i = 0; i < 3; i++){
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	if(first_f.digits.size() % 4 == 2)
		for(int i = 0; i < 2; i++){
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	if(first_f.digits.size() % 4 == 3)first_f.digits.emplace(first_f.digits.begin(), 0);
	for(unsigned int i = 0; i < first_f.digits.size(); i += 4){
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('0'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('1'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('2'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('3'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('4'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('5'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('6'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('7'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('8'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('9'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('A'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('B'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('C'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('D'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('E'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('F'); } } }
	}

	while(result[0] == '0'){
		result.erase(0, 1);
		if(result.length() == 1)break;
	}
	return result;
}

int cmp(GF_2 first_f, GF_2 second_f){
	if(first_f.digits.size() > second_f.digits.size()){
		int d = first_f.digits.size() - second_f.digits.size();
		for(int i = 0; i < d; i++){
			second_f.digits.emplace(second_f.digits.begin(), 0);
		}
	}
	if(first_f.digits.size() < second_f.digits.size()){
		int d = second_f.digits.size() - first_f.digits.size();
		for(int i = 0; i < d; i++){
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	}
	unsigned int i = 0;
	while(first_f.digits[i] == second_f.digits[i])
	{
		i++;
		if(i == first_f.digits.size())return 0;
	}
	if(first_f.digits[i] > second_f.digits[i])return 1;
	else return -1;
}

int gen_cmp(GF_2 first_f){
	GF_2 gen;
	gen.gen_set();
	first_f.remove_zeros();
	if(first_f.digits.size() >= gen.digits.size())
		return 1;
	else
		return -1;
}

GF_2 shift(GF_2 first_f, int a){
	for(int i = 0; i < a; i++){
		first_f.digits.push_back(0);
	}
	if(first_f.digits.size() % 4 == 1)
		for(int i = 0; i < 3; i++){
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	if(first_f.digits.size() % 4 == 2)
		for(int i = 0; i < 2; i++){
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	if(first_f.digits.size() % 4 == 3)first_f.digits.emplace(first_f.digits.begin(), 0);
	return first_f;
}

GF_2 mod_gen(GF_2 first_f){
	GF_2 gen, result, temp;
	gen.gen_set();
	int gen_l = gen.digits.size();
	result = first_f;
	if(gen_cmp(result) == -1)return result;
  else{
	 while(gen_cmp(result) == 1){
		 int result_l = result.digits.size();
		 temp = shift(gen, result_l - gen_l);
		 result = add(result, temp);
		 result.remove_zeros();
	 }
  }
	return result;
}

GF_2 sq(GF_2 first_f){
	GF_2 second_f;
	second_f.constOne();
	if(cmp(first_f, second_f) == 0){
		first_f.remove_zeros();
		return first_f;
	}
	else{
		first_f.remove_zeros();
		for(unsigned int i = 1; i <= first_f.digits.size()-1; i += 2){
			first_f.digits.insert(first_f.digits.begin() + i, 0);
		}
		first_f = mod_gen(first_f);
		do{
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}while((first_f.digits.size() % 4) > 0);
		return first_f;
	}
}

GF_2 mul(GF_2 first_f, GF_2 second_f){
	GF_2 result;
	int d;
	if(first_f.digits.size() > second_f.digits.size()){
		d = first_f.digits.size() - second_f.digits.size();
		for(int i = 0; i < d; i++){
			second_f.digits.emplace(second_f.digits.begin(), 0);
		}
	}
	if(first_f.digits.size() < second_f.digits.size()){
		d = second_f.digits.size() - first_f.digits.size();
		for(int i = 0; i < d; i++){
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	}

	for(unsigned int i = 0; i < 2*first_f.digits.size(); i++){
		result.digits.push_back(0);
	}
	for(unsigned int i = first_f.digits.size() - 1; i > 0; i--){
		for(unsigned int j = 0; j <= first_f.digits.size() - 1; j++)
			result.digits[i + j] = (result.digits[i + j] + first_f.digits[i] * second_f.digits[j]) % 2;
	}
	result.digits.pop_back();
	result = mod_gen(result);
	do{
		result.digits.emplace(result.digits.begin(), 0);
	}while((result.digits.size() % 4) > 0);
	return result;
}

GF_2 pow(GF_2 first_f, GF_2 second_f){
	GF_2 result("1"),one,zero;
	zero.constZero();
	one.constOne();
	if(cmp(second_f, zero) == 0)return one;
	else if(cmp(second_f, one) == 0)return first_f;
	for(int i = second_f.digits.size() - 1 ; i >= 0; i--){
		if(second_f.digits[i] == 1)
			result = mul(result, first_f);
		first_f = sq(first_f);
	}
	do{
		result.digits.emplace(result.digits.begin(), 0);
	}while((result.digits.size() % 4) > 0);
	return result;
}

GF_2 tr(GF_2 first_f){
	GF_2 tr("0"),gen;
	gen.gen_set();
	for(int i = 0; i <= 232; i++){
		tr = add(tr, first_f);
		first_f = sq(first_f);
	}
	return tr;
}

GF_2 ob_f(GF_2 first_f){ //не совсем понимаю как работает это говно
												 //я просто сделал по алгоритму
	GF_2 result("1"),temp1,temp2("1");
	temp1 = pow(first_f, temp2);
	for(unsigned int i = 0 ; i <= 233 - 2; i++){
		result = mul(result, temp1);
		temp1 = sq(temp1);
	}
	result = sq(result);
	do{
		result.digits.emplace(result.digits.begin(), 0);
	}while((result.digits.size() % 4) > 0);
	return result;
}
