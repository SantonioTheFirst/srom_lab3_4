#include <string>
#include <vector>
#include <iostream>
using namespace std;

class LambdaMatrix{
  public:
    int array[233][233];
  	LambdaMatrix();
  	~LambdaMatrix();
  	void show_lambda_matrix();
};

class GF_2 {
  protected:
  	string n;
  public:
  	vector<int> digits;
  	GF_2();
  	GF_2(string n);
  	GF_2 remove_zeros();
  	~GF_2();
  	GF_2& constZero();
  	GF_2& constOne();
};

LambdaMatrix::LambdaMatrix(){
	int p = 2 * 233 + 1;
	for(int i = 0; i <= 233 - 1; i++){
		for(int j = 0; j <= 233 - 1; j++){
			int pow_2i = mod_pow(2, i, p);
			int pow_2j = mod_pow(2, j, p);
			int a1 = (pow_2i + pow_2j) % p;
			int a2 = (-pow_2i + pow_2j) % p;
			int a3 = (pow_2i - pow_2j) % p;
			int a4 = (-pow_2i - pow_2j) % p;
			if(a1 == 1 || a2 == 1 || a3 == 1 || a4 == 1)array[i][j] = 1;
			else{
				array[i][j] = 0;
			}
		}
	}
}

LambdaMatrix::~LambdaMatrix(){}

void LambdaMatrix::show_lambda_matrix(){
	for(unsigned int i = 0; i <= 233 - 1; i++){
    cout << endl;
		for(unsigned int j = 0; j <= 233 - 1; j++)
			cout << array[i][j];
	}
}

int mod_pow(int first, int second, int p){
	int t = first;
	if(second == 0) return 1;
	if(second == 1) return first;
	for(int i = 1; i < second; i++)
	{
		first = t * first;
		first = first % p;
	}
	return first;
}

GF_2::GF_2(){}

GF_2::GF_2(string n){
	for(int i = 0; i <= n.length(); i++)
	{
		if(n[i] == '0'){ digits.push_back(0); digits.push_back(0); digits.push_back(0); digits.push_back(0); }
		if(n[i] == '1'){ digits.push_back(0); digits.push_back(0); digits.push_back(0); digits.push_back(1); }
		if(n[i] == '2'){ digits.push_back(0); digits.push_back(0); digits.push_back(1); digits.push_back(0); }
		if(n[i] == '3'){ digits.push_back(0); digits.push_back(0); digits.push_back(1); digits.push_back(1); }
		if(n[i] == '4'){ digits.push_back(0); digits.push_back(1); digits.push_back(0); digits.push_back(0); }
		if(n[i] == '5'){ digits.push_back(0); digits.push_back(1); digits.push_back(0); digits.push_back(1); }
		if(n[i] == '6'){ digits.push_back(0); digits.push_back(1); digits.push_back(1); digits.push_back(0); }
		if(n[i] == '7'){ digits.push_back(0); digits.push_back(1); digits.push_back(1); digits.push_back(1); }
		if(n[i] == '8'){ digits.push_back(1); digits.push_back(0); digits.push_back(0); digits.push_back(0); }
		if(n[i] == '9'){ digits.push_back(1); digits.push_back(0); digits.push_back(0); digits.push_back(1); }
		if((n[i] == 'A') || (n[i] == 'a')){ digits.push_back(1); digits.push_back(0); digits.push_back(1); digits.push_back(0); }
		if((n[i] == 'B') || (n[i] == 'b')){ digits.push_back(1); digits.push_back(0); digits.push_back(1); digits.push_back(1); }
		if((n[i] == 'C') || (n[i] == 'c')){ digits.push_back(1); digits.push_back(1); digits.push_back(0); digits.push_back(0); }
		if((n[i] == 'D') || (n[i] == 'd')){ digits.push_back(1); digits.push_back(1); digits.push_back(0); digits.push_back(1); }
		if((n[i] == 'E') || (n[i] == 'e')){ digits.push_back(1); digits.push_back(1); digits.push_back(1); digits.push_back(0); }
		if((n[i] == 'F') || (n[i] == 'f')){ digits.push_back(1); digits.push_back(1); digits.push_back(1); digits.push_back(1); }
	}
}

GF_2::~GF_2(){}

GF_2 GF_2::remove_zeros(){
	if(this->digits.size() == 1)return *this;
	else{
		while(this->digits[0] == 0){
			this->digits.erase(this->digits.begin());
			if(this->digits.size() == 1)break;
		}
		return *this;
	}
}

GF_2& GF_2::constZero(){
	GF_2* first_f = new GF_2("0");
	return *first_f;
}

GF_2& GF_2::constOne(){
	GF_2* first_f = new GF_2("1");
	return *first_f;
}

string convert_to_str(GF_2 first_f){
	string result;
	do{
		first_f.digits.emplace(first_f.digits.begin(), 0);
	}while(first_f.digits.size() % 4 > 0);
	for(unsigned int i = 0; i < first_f.digits.size(); i += 4){
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('0'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('1'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('2'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('3'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('4'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('5'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('6'); } } }
		if(first_f.digits[i] == 0){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('7'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('8'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('9'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('A'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 0){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('B'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 0) result.push_back('C'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 0){ if(first_f.digits[i + 3] == 1) result.push_back('D'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 0) result.push_back('E'); } } }
		if(first_f.digits[i] == 1){ if(first_f.digits[i + 1] == 1){ if(first_f.digits[i + 2] == 1){ if(first_f.digits[i + 3] == 1) result.push_back('F'); } } }
	}
	while(result[0] == '0'){
		result.erase(0, 1);
		if(result.length() == 1)break;
	}
	return result;
}

GF_2 add_zeros(GF_2 first_f){
	first_f.remove_zeros();
	int a = 233 - first_f.digits.size();
	for(int i = 0; i < a; i++)
		first_f.digits.emplace(first_f.digits.begin(), 0);
	return first_f;
}

GF_2 add(GF_2 first_f, GF_2 second_f){
	GF_2 result;
	int d;
	if(first_f.digits.size() > second_f.digits.size()){
		d = first_f.digits.size() - second_f.digits.size();
		for(unsigned int i = 0; i < d; i++){
			second_f.digits.emplace(second_f.digits.begin(), 0);
		}
	}
	if(first_f.digits.size() < second_f.digits.size()){
		d = second_f.digits.size() - first_f.digits.size();
		for(unsigned int i = 0; i < d; i++){
			first_f.digits.emplace(first_f.digits.begin(), 0);
		}
	}
	for(unsigned int i = 0; i < first_f.digits.size(); i++)
		result.digits.push_back((first_f.digits[i] ^ second_f.digits[i]));
	return result;
}

GF_2 shift_r(GF_2 first_f){
	first_f = add_zeros(first_f);
	int t = first_f.digits[232];
	for(unsigned int i = 232; i > 0; i--){
		first_f.digits[i] = first_f.digits[i - 1];
	}
	first_f.digits[0] = t;
	return first_f;
}

GF_2 shift_r_n(GF_2 first_f, int n){
	GF_2 second_f("0");
	first_f = add_zeros(first_f);
	second_f = add_zeros(second_f);
	for(unsigned int i = 0; i <= first_f.digits.size() - 1 - n; i++){
		second_f.digits[i + n] = first_f.digits[i];
	}
	for(int j = 0; j < n; j++){
		second_f.digits[j] = first_f.digits[j + (first_f.digits.size() - n)];
	}
	return second_f;
}

GF_2 shift_l(GF_2 first_f){
	first_f = add_zeros(first_f);
	int t = first_f.digits[0];
	for(unsigned int i = 0; i < 232; i++){
		first_f.digits[i] = first_f.digits[i + 1];
	}
	first_f.digits[232] = t;
	return first_f;
}

GF_2 shift_l_n(GF_2 first_f, int n){
	GF_2 second_f("0");
	first_f = add_zeros(first_f);
	second_f = add_zeros(second_f);
	for(unsigned int i = 0; i <= first_f.digits.size() - 1 - n; i++){
		second_f.digits[i] = first_f.digits[i + n];
	}
	for(int j = 0; j < n; j++){
		second_f.digits[j + (first_f.digits.size() - n)] = first_f.digits[j];
	}
	return second_f;
}

GF_2 mul(GF_2 first_f, GF_2 second_f, LambdaMatrix l){
	GF_2 result("0"), t("0");
	first_f = add_zeros(first_f);
	second_f = add_zeros(second_f);
	result = add_zeros(result);
	t = add_zeros(t);
  cout << "BEFORE MULT CYCLE" << endl;
	int c = 0;
	for(unsigned int k = 0; k < 233; k++){
		for(unsigned int i = 0; i < 233; i++){
			for(unsigned int j = 0; j < 233; j++){
				if((first_f.digits[233 - j - 1] == 1) && (l.array[i][j] == 1))c = (c + 1) & 1;
        cout << "INNER MULT CYCLE" << endl;
			}
			t.digits[i] = c;
			c = 0;
		}
		for(unsigned int i = 0; i < 233; i++){
			if((second_f.digits[233 - i - 1] == 1) && (t.digits[i] == 1))c = (c + 1) & 1;
		}
		if(c == 1){
			result.digits[233 - k - 1] = 1;
		}
		c = 0;
		first_f = shift_l(first_f);
		second_f = shift_l(second_f);
	}
  cout << "END OF MULT METHOD" << endl;
	return result;
}

GF_2 sq(GF_2 first_f){
	return shift_r(first_f);
}

int tr(GF_2  first_f){
	first_f = add_zeros(first_f);
	int tr = 0;
	for(int i = 0; i < 233; i++)
		tr = (tr + first_f.digits[i]) % 2;
	return tr;

}

GF_2 pow(GF_2 first_f, GF_2 second_f, LambdaMatrix L){
	GF_2 result;
	result.constOne();
	first_f = add_zeros(first_f);
	second_f = add_zeros(second_f);
	for(int i = 233 - 1; i >= 0; i--){
		if(second_f.digits[i] == 1){
			result = mul(result, first_f, L);
		}
		first_f = sq(first_f);
	}
	return result;
}

GF_2 inverse(GF_2 first_f,LambdaMatrix L){
	GF_2 m("233");
	GF_2 t,result;
	t = first_f;
	m.remove_zeros();
	result.constOne();
	int k = 1, t = m.digits.size();
	for(int i=t - 1; i >= 0; i--){
		t = mul(shift_r_m(t, k), t, L);
		k = 2 * k;
		if(m.digits[i] == 1){
			t = (sq(t), first_f);
			k = k + 1;
		}
	}
	result = sq(t);
	return result;
}
